import FreeCAD
import FreeCADGui
import Part
import os

LOCAL_PATH = os.path.dirname(os.path.abspath(__file__))
DISPLAY = True


class TestFiles:
    """Convenience class to grab test file paths"""
    # 2 through countersinks
    simple_1 = os.path.join(LOCAL_PATH, 'parts', 'simple_1.step')
    
    # 1 through counterbore
    simple_2 = os.path.join(LOCAL_PATH, 'parts', 'simple_2.step')
    
    # 0 basic, 12 counter bores, 13 countersinks, all through
    simple_3 = os.path.join(LOCAL_PATH, 'parts', 'simple_3.step')
    
    # 2 basic, 4 blind countersinks
    intermediate_1 = os.path.join(LOCAL_PATH, 'parts', 'intermediate_1.step')
    
    # 6 basic, 2 countersinks, all through (some off axis!)
    intermediate_2 = os.path.join(LOCAL_PATH, 'parts', 'intermediate_2.step')
    
    # 8 blind countersinks, 10 through counterbores, 4 through basic
    intermediate_3 = os.path.join(LOCAL_PATH, 'parts', 'intermediate_3.step')
    
    # Depends on your solution, have fun with it!
    challenge_1 = os.path.join(LOCAL_PATH, 'parts', 'challenge_1.step')


class Hole:
    """Hole class is based around a list of circles with the same center point"""
    circle_list = []
    def __init__(self, c_list):
        circle_list = c_list
    def center_point(self):
        #returns the center point of the hole with the first circle
        #converted to 2D coordinates to account for the center of all the circle being on the same line
		#if (circle_list[0]).Center
        return (circle_list[0]).Center
    def depth(self):
        return 0
    def type(self):
        return 0
    def composition(self):
        if len(circle_list) == 2:
            return "normal"
        elif len(circle_list) == 3:
            return "countersink"
        elif len(circle_list) == 4:
            return "counterbore"
        else:
            return "unknown"

def display(obj, name, color=(0.5, 0.5, 0.5), transparency=0, visibility=True):
    """
    Utility function to display a face or group of faces
    
    :param obj: Either a single ``Part.Face``, a list or set of ``Part.Face`` 
    objects, or a ``Part.Shape`` object
    :param name: ``string`` Name to give the face or group of faces
    :param color: Three-tuple of RGB color values for the face(s) display 
    color. Must be floats so make sure the numbers in the tuple have a decimal 
    point at the end as seen in the inputs. Default color is gray
    :param transparency: Number between 0 and 1, determines how see through 
    the part is, default to 0
    :param visibility: ``Boolean`` sets default visibility of object, can be 
    toggled with space bar in FreeCAD
    """
    # check to see if a document has been created, if not, make one
    if FreeCADGui.ActiveDocument is None:
        FreeCAD.newDocument()

    if isinstance(obj, Part.Face):
        FreeCAD.ActiveDocument.addObject('Part::Feature', name).Shape = obj
    elif isinstance(obj, list) or isinstance(obj, set):
        sh = Part.makeShell(list(obj))
        FreeCAD.ActiveDocument.addObject('Part::Feature', name).Shape = sh
    elif isinstance(obj, Part.Shape):
        FreeCAD.ActiveDocument.addObject('Part::Feature', name).Shape = obj
    else:
        print('Incompatible display type!')
        return

    # Now set the color and transparency.
    # After creating the object, it is always set to the Gui active object;
    # object can also be referenced with the syntax:
    # FreeCADGui.ActiveDocument.getObject(name)
    FreeCADGui.ActiveDocument.ActiveObject.DiffuseColor = color
    FreeCADGui.ActiveDocument.ActiveObject.Transparency = transparency
    FreeCADGui.ActiveDocument.ActiveObject.Visibility = visibility


def interrogate(path_to_file):
    """
    Implementation of hole detection logic. Put your solution here! You can
    remove the example logic below.
    
    :param path_to_file: ``string`` path to the part file, use ``TestFiles`` 
    for convenience
    :return: List of dictionaries of hole information
    """

    # code to read the part into shape
    shape = Part.Shape()
    shape.read(path_to_file)

    # refer to this face list for indices
    face_list = list(shape.Faces)

    # you should output a list of holes
    output_info = []

    # call the function to find all the circles
    circles_found = find_circles(shape)

    #compare the centers of the circles in circles_found to group them into a list of lists of circles
    grouped_circles = group_into_holes(circles_found)

    #make the list of lists of circles into hole objects
    for x in grouped_circles:
        output_info.append(Hole(x))

    # example of how to use the display function
    # only display if DISPLAY is True
    if DISPLAY:
        example_list = [f for f in shape.Faces
                        if isinstance(f.Surface, Part.Cylinder)]
        display(shape, 'SolidBody', visibility=True)
        display(example_list, 'SampleGroupingOfFaces', color=(0., 0.5, 0.))
        display(shape.Faces[0], 'SampleSingleFace', color=(0., 0., 0.5),
                visibility=False)

    # this fits the display to the items that you just displayed
    FreeCADGui.SendMsgToActiveView('ViewFit')

    # now return the output dictionary
    return output_info


def find_circles(shape):
    # narrows down the list of edges to the list of circles
    # :param shape: Shape that we are looking at
    # :return: list of holes
    edge_list = list(shape.Edges)
    circle_list = []
    for x in range(len(edge_list)):
        temp = (edge_list[x]).Curve
        if isinstance(temp, Part.Circle):
            for y in circle_list:
                #it is double counting the circles by parsing them as two arcs for some reason
                #either compare radii, center points, and axes or find out if they intersect at either end
    return circle_list

def group_into_holes(list_of_circles):
    # runs the three tests to determine if circle objects correspond to the same hole
    # :param list_of_circles: list of Circle objects
    # :return: list of list of circles
    num_circles = len(list_of_circles)
    list_of_holes = []
    while num_circles > 0:
        # while there are items in the list
        first = list_of_circles[0]
        rest = list_of_circles[1:]
        first_test = compare_axes(first, rest)
        if len(first_test) > 1:
            rest = first_test
            second_test = compare_planes(first, rest)
            if len(second_test) > 1:
                rest = second_test
                last_test = compare_centers(first,rest)
                if len(last_test) > 1:
                    list_of_holes.append(last_test)
                    for y in last_test:
                        if y in list_of_circles: list_of_circles.remove(y)
        if first in list_of_circles: list_of_circles.remove(first)
        num_circles = len(list_of_circles)
    return list_of_holes

def compare_centers(first, list_of_rest):
    #takes the first circle and compares its center point to the rest of the lists'
    #this should be done knowing that the circles have different, but parallel planes
    axis = first.Axis
    center1 = first.Center
    temp_list = [first]
    for x in list_of_rest:
        center2 = x.Center
        #if t is an int in (x,y,z) - (x0,y0,z0) = t(a,b,c)
        #where (a,b,c) is the axis and the others are the centers
        s = [(center1[0]-center2[0]),(center1[1]-center2[1]), (center1[2]-center2[2])]
        for y in range(len(s)):
            if s[y] == 0:
                s[y] = 1
        for z in range(len(axis)):
            if axis[z] == 0:
                axis[z] = float(1)
        if s[0]%axis[0] == s[1]%axis[1] == s[2]%axis[2]:
            temp_list.append(x)
    return temp_list

def compare_planes(first, list_of_rest):
    #if the circles have the same plane equation (d = ax0 + byo + czo),
    #then they are NOT associated with the same hole
    #knowing that they have the same normal vector
    center1 = first.Center
    axis1 = first.Axis
    d1 = center1[0]*axis1[0]+center1[1]*axis1[1]+center1[2]*axis1[2]
    temp_list = [first]
    for x in range(len(list_of_rest)):
        center2 = (list_of_rest[x]).Center
        axis2 = (list_of_rest[x]).Axis
        d2 = center2[0]*axis2[0]+center2[1]*axis2[1]+center2[2]*axis2[2]
        if d1 != d2:
            temp_list.append(list_of_rest[x])

    return temp_list

def compare_axes(first, list_of_rest):
    #takes the first circle in a list and compares its axis to the rest of the lists'
    #if they share an axis, then they operate in the same or parallel planes
    #returns the list with the first circle and containing any with the same axis
    axis1 = first.Axis
    temp_list = [first]
    for x in range(len(list_of_rest)):
        axis2 = (list_of_rest[x]).Axis
        if axis1 == axis2:
            temp_list.append(list_of_rest[x])
    return temp_list