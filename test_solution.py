import solution
import unittest


class TestSolution(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        solution.DISPLAY = False

    def test_simple_1(self):
        holes = solution.interrogate(solution.TestFiles.simple_1)
        self.assertEqual(2, len(holes))

    def test_simple_2(self):
        holes = solution.interrogate(solution.TestFiles.simple_2)
        self.assertEqual(1, len(holes))

    def test_simple_3(self):
        holes = solution.interrogate(solution.TestFiles.simple_3)
        self.assertEqual(25, len(holes))

    def test_intermediate_1(self):
        holes = solution.interrogate(solution.TestFiles.intermediate_1)
        self.assertEqual(6, len(holes))

    def test_intermediate_2(self):
        holes = solution.interrogate(solution.TestFiles.intermediate_2)
        self.assertEqual(8, len(holes))

    def test_intermediate_3(self):
        holes = solution.interrogate(solution.TestFiles.intermediate_3)
        self.assertEqual(22, len(holes))

    def test_challenge_1(self):
        holes = solution.interrogate(solution.TestFiles.challenge_1)
        self.assertGreaterEqual(len(holes), 22)


def run_tests():
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSolution)
    unittest.TextTestRunner(verbosity=2).run(suite)

