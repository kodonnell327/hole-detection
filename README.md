# Paperless Parts Coding Challenge #

This is a challenging exercise that will introduce you to a basic computational 
geometry technique. If we've asked you to complete this challenge as part of 
the employment application process, Paperless Parts will consider your solution 
as part of our evaluation of your technical skills and problem solving capabilities.

A robust solution to this challenge would take a long time to implement! This is hard
problem, but we are not looking for you to spend too much time on this. Decide how much 
time you can work on this, and see how far you can get. Communicate the number of hours
you worked on it and what you would do if you had more time.

Again, we are not concerned about a robust, complete solution. Some of questions we're 
looking to answer: Are you able to use Git and BitBucket? Did you get FreeCAD installed?
Could you run a Python script through the FreeCAD console? Did you document solution
and submit in an effective way?


## Project Goals ##

For this project, you will use an open source CAD tool known as FreeCAD to 
extract certain geometric features from three dimensional part files. 
FreeCAD has a convenient Python scripting console that allows us to interact 
with geometries and FreeCAD features via Python commands. To solve this challenge, 
you will be writing Python code and communicating your work to us through the use 
of git.

#### Basic Objective ####

Your task is to write a function that analyzes a 3D solid object and counts 
the number of drilled holes.

As you'll see from the example 3D objects, the term "drilled holes" is somewhat
ambiguous. You may define this however you'd like, but be sure to document your
definition.

(See hints below!)

#### Extending Your Solution ####

If you've successfully detected holes in the test files, you can extend your
solution by classifying these holes. For each hole, can you determine:

* Type (blind or through)
* Composition (basic, countersink, counterbore)
* Diameter (the smallest diameter in the feature)
* Depth (the depth of the entire feature)


## Getting Started ##

#### Prerequisites: ####

* Install FreeCAD (latest version can be downloaded here: [https://www.freecadweb.org](https://www.freecadweb.org)).
* Create a free BitBucket account ([https://www.bitbucket.com](https://www.bitbucket.com)).
* Install Python 2.7 and familiarize yourself with lists, dictionaries, and sets.
* Optional: Install PyCharm IDE ([https://www.jetbrains.com/pycharm/](https://www.jetbrains.com/pycharm/)).
* Get a Mac or Linux terminal and some basic familiarity with it. For Windows, consider installing and learning how to use Cygwin.

#### Fork and clone this repository: ####

* Fork this repository using to create your own repository belonging to your BitBucket account. You may choose to make this a private repository if you don't want your solution to by publicly available.
* Clone your fork (for example: `git clone https://username@bitbucket.org/username/hole-detection.git`).
* Note the full path of this clone (for example, `/Users/username/path/to/hole-detection`).

For more detailed instructions on how to fork a repository in BitBucket, see [Forking a Repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html).

`solution.py` contains a skeleton function `interrogate()`, which is where you will put your 
solution. `test_solution.py` is the test suite we will use to help us evaluate your solution.

## Run Your Solution in FreeCAD ##

Before you can implement a solution, you will need to get familiar with loading a part and 
interacting with it in Python. First show the Python Console in FreeCAD by going to View > Panels > Python Console. From there, you can display a STEP file by running these commands in the console
(you'll need to substitute `/Users/username/path/to/hole-detection/` with the location where
you cloned the repository):

    import Part
    shape = Part.Shape()
    shape.read('/Users/username/path/to/hole-detection/parts/simple_1.step')
    Part.show(shape)
    FreeCADGui.SendMsgToActiveView('ViewFit')

*Hint: `shape.Faces` is a list of the faces of this 3D part.*

When you're working on your solution, you should test early and often by running your
`interrogate()` function. To run your solution, first run these lines:

    import sys
    sys.path.append('/Users/username/path/to/hole-detection')
    import solution

Each time you want to run your solution, type or paste this line:

    reload(solution); solution.interrogate(solution.TestFiles.simple_1)

> Note, you can replace `solution.TestFiles.simple_1` with any of the available test files.

As you debug your solution, it may be helpful to visualize the part of a subset of its faces.
We have provided a function `solution.display()` to help you. For details on how to use this 
function, see the source documentation or run `help(solution.display)` in the FreeCAD 
Python console.

To run your solution against all of the available test files, type or paste these lines:

    import test_solution
    test_solution.run_tests()

If you feel the need for additional test cases to highlight the capabilities of your solution,
feel free to create and add your own.

When you are satisfied with your solution's performance, submit a pull request to Paperless Parts.
(See instructions in the next section.)

## Contribution Guidelines ##

You will submit your solution using git and BitBucket. You should include a written description
of your summary in any form you find effective. Options include inline code comments, pull request
comments, a dedicated "readme" file, or even an email.

It is a good idea to checkpoint your work regularly using git. Do this as follows:

* View the current state of your clone: `git status`
* Stage changed files for commit: `git add -u`
* If you've created new files, add them: `git add my-new-file.py`
* Commit your changes: `git commit -m "My awesome description of this commit"`
* Upload (i.e., "push") your changes to BitBucket: `git push origin master`

When you are ready to submit your solution, go to your repository on BitBucket website and create a new Pull Request. You can find more information here: [Create a Pull Request](https://confluence.atlassian.com/bitbucket/create-a-pull-request-774243413.html).

## Recommendations ##

Start by taking a look at the sample parts in the `parts` folder. This will give you an idea
of what the holes you're trying to detect look like.

Spend some type digging into the FreeCAD syntax, particularly with the Part module. In the Python console, enter `help(Part)`, `help(Part.Face)`, and `help(Part.Edge)`. These will give you basic descriptions on all of the functions and information you have access to when referring to geometric features of the part file. Exploring the capabilities will be very useful when beginning to brainstorm.

Start simple! Can you identify a simple hole in any of the test parts? Hint: what type of face is
associated with a hole?

It's easy to get lost in a challenge like this. Give yourself time limits, see how far you can get,
and let us know if you have questions or get stuck.
